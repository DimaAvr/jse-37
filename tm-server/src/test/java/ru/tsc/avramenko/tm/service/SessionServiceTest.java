package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.avramenko.tm.api.service.IServiceLocator;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.repository.SessionRepository;
import ru.tsc.avramenko.tm.repository.UserRepository;

import java.util.Date;
import java.util.List;

public class SessionServiceTest {

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Session session;

    @NotNull
    protected static final String TEST_USER_LOGIN = "TestLogin";

    @NotNull
    protected static final String TEST_USER_PASSWORD = "TestUserPassword";

    @NotNull
    protected static final String TEST_USER_PASSWORD_INCORRECT = "TestUserPasswordIncorrect";

    @NotNull
    protected static final String TEST_USER_ID_INCORRECT = "647";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionService = new SessionService(connectionService, bootstrap);
        this.session = sessionService.open("Test", "Test");
    }

    @After
    public void after() {
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());

        @NotNull final Session sessionById = sessionService.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session.getId(), sessionById.getId());
    }

    @Test
    public void findById() {
        @NotNull final Session session = sessionService.findById(this.session.getId());
        Assert.assertNotNull(session);
    }

    @Test
    public void findByIdIncorrect() {
        @NotNull final Session session = sessionService.findById(TEST_USER_ID_INCORRECT);
        Assert.assertNull(session);
    }

    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        sessionService.findById(null);
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Session> session = sessionService.findAll();
        Assert.assertNotNull(session);
        Assert.assertEquals(1, session.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Session> session = sessionService.findAll();
        Assert.assertNotNull(session);
        Assert.assertEquals(1, session.size());
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        sessionService.removeById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        sessionService.removeById(null);
        Assert.assertNotNull(sessionService.findById(session.getId()));
    }

    @Test
    public void removeByIdIncorrect() {
        sessionService.removeById(TEST_USER_ID_INCORRECT);
        Assert.assertNotNull(sessionService.findById(session.getId()));
    }

    @Test(expected = AccessDeniedException.class)
    public void openIncorrect() {
        sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD_INCORRECT);
        Assert.assertNotNull(session);
    }

    @Test
    public void validate() {
        sessionService.validate(session);
        Assert.assertNotNull(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateChanged() {
        @Nullable final Session session = sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        session.setSignature("647");
        sessionService.validate(session);
    }

}