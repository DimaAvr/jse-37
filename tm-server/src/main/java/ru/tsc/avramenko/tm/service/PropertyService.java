package ru.tsc.avramenko.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.service.IPropertyService;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String BACKUP_INTERVAL_KEY = "backup.interval";

    @NotNull
    private static final String BACKUP_INTERVAL_DEFAULT = "30";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "23";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "132";

    @NotNull
    private static final String SESSION_SECRET_KEY = "session.secret";

    @NotNull
    private static final String SESSION_SECRET_DEFAULT = "112";

    @NotNull
    private static final String SESSION_ITERATION_KEY = "session.iteration";

    @NotNull
    private static final String SESSION_ITERATION_DEFAULT = "113";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8081";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String JDBC_USER_DEFAULT = "admin";

    @NotNull
    private static final String JDBC_USER_KEY = "jdbc.user";

    @NotNull
    private static final String JDBC_PASS_DEFAULT = "admin";

    @NotNull
    private static final String JDBC_PASS_KEY = "jdbc.password";

    @NotNull
    private static final String JDBC_URL_DEFAULT = "jdbc:mysql://localhost/task-manager";

    @NotNull
    private static final String JDBC_URL_KEY = "jdbc.url";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    private String getValue(final String name, final String defaultValue) {
        @Nullable
        final String systemProperty = System.getProperty(name);
        if (systemProperty != null) return systemProperty;
        @Nullable
        final String environmentProperty = System.getenv(name);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(name, defaultValue);
    }

    private int getValueInt(final String name, final String defaultValue) {
        @Nullable
        final String systemProperty = System.getProperty(name);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable
        final String environmentProperty = System.getenv(name);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(properties.getProperty(name, defaultValue));
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return getValueInt(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getBackupInterval() {
        return getValueInt(BACKUP_INTERVAL_KEY, BACKUP_INTERVAL_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getValueInt(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSignatureSecret() {
        return getValueInt(SESSION_SECRET_KEY, SESSION_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSignatureIteration() {
        return getValueInt(SESSION_ITERATION_KEY, SESSION_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getJdbcUser() {
        return getValue(JDBC_USER_KEY, JDBC_USER_DEFAULT);
    }

    @NotNull
    @Override
    public String getJdbcPass() {
        return getValue(JDBC_PASS_KEY, JDBC_PASS_DEFAULT);
    }

    @NotNull
    @Override
    public String getJdbcUrl() {
        return getValue(JDBC_URL_KEY, JDBC_URL_DEFAULT);
    }

}