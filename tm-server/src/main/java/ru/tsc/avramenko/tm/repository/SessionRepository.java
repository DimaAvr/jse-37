package ru.tsc.avramenko.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.ISessionRepository;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    protected String getTableName() {
        return "app_session";
    }


    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        @NotNull final String query =
                "SELECT * FROM " + getTableName() + " " +
                        "WHERE id = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        final boolean result = statement.executeQuery().first();
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session add(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final String query = "insert into " + getTableName() +
                " (id, timestamp, signature, user_id) " +
                "values(?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setLong(2, session.getTimestamp());
        statement.setString(3, session.getSignature());
        statement.setString(4, session.getUserId());
        statement.executeUpdate();
        statement.close();
        return session;
    }

    @Override
    @SneakyThrows
    protected Session fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setUserId(row.getString("user_id"));
        session.setTimestamp(row.getLong("timestamp"));
        session.setId(row.getString("id"));
        session.setSignature(row.getString("signature"));
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session update(@Nullable final Session entity) {
        if (entity == null) return null;
        @NotNull final String query = "update " + getTableName() +
                " id=?, timestamp=?, signature=?, user_id=? where id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setLong(2, entity.getTimestamp());
        statement.setString(3, entity.getSignature());
        statement.setString(4, entity.getUserId());
        statement.setString(5, entity.getId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

}