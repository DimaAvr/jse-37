package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.IService;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.User;

public interface ISessionService extends IService<Session> {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    @Nullable Session sign(@Nullable Session session);

    @Nullable
    void close(@NotNull Session session);

    @Nullable
    Session open(@Nullable String login, @Nullable String password);

    void validate(@NotNull Session session, @Nullable Role role);

    void validate(@Nullable Session session);

}