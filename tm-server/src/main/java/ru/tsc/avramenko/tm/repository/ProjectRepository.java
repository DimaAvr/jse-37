package ru.tsc.avramenko.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.IProjectRepository;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.util.stream.Collectors;

public class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull Connection connection) {
        super(connection);
    }

    protected String getTableName() {
        return "app_project";
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project add(@Nullable final Project entity) {
        if (entity == null) return null;
        @NotNull final String query = "insert into " + getTableName() +
                " (id, name, description, status, start_date, finish_date, created_date, user_id) " +
                "values(?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setTimestamp(5, prepare(entity.getStartDate()));
        statement.setTimestamp(6, prepare(entity.getFinishDate()));
        statement.setTimestamp(7, prepare(entity.getCreated()));
        statement.setString(8, entity.getUserId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project add(@NotNull final String userId, @Nullable final Project entity) {
        if (entity == null) return null;
        entity.setUserId(userId);
        @NotNull final String query = "insert into " + getTableName() +
                " (id, name, description, status, start_date, finish_date, created_date, user_id) " +
                "values(?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setTimestamp(5, prepare(entity.getStartDate()));
        statement.setTimestamp(6, prepare(entity.getFinishDate()));
        statement.setTimestamp(7, prepare(entity.getCreated()));
        statement.setString(8, userId);
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    @SneakyThrows
    protected Project fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setId(row.getString("id"));
        project.setUserId(row.getString("user_id"));
        project.setStartDate(row.getDate("start_date"));
        project.setFinishDate(row.getDate("finish_date"));
        project.setCreated(row.getDate("created_date"));
        return project;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findById(userId, id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE name = ? AND user_id=? limit 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final Project result = fetch(resultSet);
        statement.close();
        return result;

    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findByIndex(@NotNull final String userId, @NotNull final int index) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id=? limit 1 offset ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(2, index - 1);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final Project result = fetch(resultSet);
        statement.close();
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE name = ? AND user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @NotNull final int index) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? offset ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index - 1);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project update(@Nullable final Project entity) {
        if (entity == null) return null;
        @NotNull final String query = "update " + getTableName() +
                " set id=?, name=?, description=?, status=?, start_date=?, finish_date=?, created_date=?, user_id=? WHERE id=? AND user_id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setTimestamp(5, prepare(entity.getStartDate()));
        statement.setTimestamp(6, prepare(entity.getFinishDate()));
        statement.setTimestamp(7, prepare(entity.getCreated()));
        statement.setString(8, entity.getUserId());
        statement.setString(9, entity.getId());
        statement.setString(10, entity.getUserId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

}