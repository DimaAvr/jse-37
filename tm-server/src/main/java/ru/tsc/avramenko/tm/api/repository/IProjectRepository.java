package ru.tsc.avramenko.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Project;
import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IOwnerRepository<Project> {

    boolean existsById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project findByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    void removeByName(@NotNull String userId, @NotNull String name);

    @Nullable
    void removeByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    @SneakyThrows
    Project update(@Nullable Project entity);

}
