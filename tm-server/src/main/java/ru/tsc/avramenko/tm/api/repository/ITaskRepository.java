package ru.tsc.avramenko.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    boolean existsById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<Task> findAllTaskByProjectId(@NotNull String userId, @NotNull final String id);

    @Nullable
    Task findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task findByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    void removeByName(@NotNull String userId, @NotNull String name);

    @Nullable
    void removeByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    Task update(@Nullable Task entity);

    @NotNull
    void unbindAllTaskByProjectId(@NotNull String userId, @NotNull final String id);

}
